﻿using System;
using System.Data.SQLite;
// []

//xxxx-1234-1234 

namespace program
{
    class Program
    {
        static void Main(string[] args)
        {
            //Varriable Declaration 
            String FirstSegment="", SecondSegment="", ThirdSegment="";
            String BarCode; 
            String VerifiedBarCode; 
            bool BarCodeIsValid = true; 
            string cs = @"URI=file:Sauvegarde.db";
            using var con = new SQLiteConnection(cs);

            //BarCode Reader 
            //Simulate the using of the standar library to read the BarCode 
            //IN : None 
            //OUT : The Bar Code 
            String BarCodeReader() 
            {
                BarCode = "Test-1234-5678"; 
                return BarCode; 
            }
            
            //The Initialisation function in C# isn't needed, so I don't have done it as I thougth 
            String Initialising(String Array2Copy) 
            {
                return Array2Copy;
            }

            //Cutting Function 
            //Cut the BarCode into three part to verify them 
            //IN the BarCode to cut 
            //OUT a Boolean  
            bool Cutting(String BarCode)
            {
                try {
                    String[] TableOut;
                    TableOut = BarCode.Split('-',StringSplitOptions.None);
                    FirstSegment=Initialising(TableOut[0]);
                    SecondSegment=Initialising(TableOut[1]);
                    ThirdSegment=Initialising(TableOut[2]);
                    return true; 
                } catch(Exception e) {
                    return false; 
                }
            }

            //Use all Verifications function to return a global boolean 
            //IN : None 
            //OUT : None 
            void Verification() 
            {
                if(NumberOfSegmentVerification() == true && SegmentIsLetter(FirstSegment) == true && 
                SegmentIsNumber(SecondSegment) == true  && SegmentIsNumber(ThirdSegment) == true) 
                    BarCodeIsValid = true;
                else 
                    BarCodeIsValid = false;       
            }

            //Verify the number of package by using the return code of the Cutting Function 
            //IN : None 
            //OUT : a Boolean 
            bool NumberOfSegmentVerification()
            {
                if(Cutting(BarCode) ==  true) {
                    return true; 
                } 
                else {
                    return false; 
                }
            }

            //Verify that the segment is compose of letters 
            //IN : a Segment (String)
            //OUT : a Boolean 
            bool SegmentIsLetter(String Array2Verify)
            {
                bool flag = true; 
                try {
                    for(int counter = 0; counter < Array2Verify.Length; counter++)
                    { 
                        if(Array2Verify[counter].GetType() == typeof(char)) 
                            flag = true ; 
                        else 
                            flag = false; 
                    }
                } catch(Exception e) {
                    flag = false; 
                }
                return flag; 
            }

            //Verify that the segment is composed of numbers 
            //IN : a Segment (String)
            //OUT : a Boolean 
            bool SegmentIsNumber(String Array2Verify)
            {
                Int32 buffer; 
                bool flag = false; 
                try {
                    buffer = int.Parse(Array2Verify); //We have to Parse the Segment because he is a String by default 
                    flag = true; 
                } catch(Exception e) { 
                    flag = false; 
                }
                return flag; 
            }

            //Group all the three created segment into one 
            //IN : the three segments (Strings)
            //OUT : a String composed of them three and 2 "-"
            String Gather(String FirstSegment, String SecondSegment, String ThirdSegment)
            {
                BarCode = FirstSegment + "-" + SecondSegment + "-" + ThirdSegment; 
                return BarCode; 
            }

            
            void ConectionBdd(){
                con.Open();
                using var cmd = new SQLiteCommand(con);
                try{
                cmd.CommandText = @"CREATE TABLE Inventaire(id INTEGER PRIMARY KEY,barcode LONGINT)";
                cmd.ExecuteNonQuery();
                }catch{
                    Console.WriteLine("Table Inventaire already exist");
                }
            }
            void InsertionBDD(String BarCode){
                using var cmd = new SQLiteCommand(con);
                cmd.CommandText = "INSERT INTO Inventaire(barcode) VALUES(@barcode)";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@barcode", BarCode);
                cmd.ExecuteNonQuery();
            }
            void DeconectionBDD(){
                con.Close();
            }

            //Exemple of utilisation : 
            BarCode = BarCodeReader();  //Initialize BarCode 
            Console.WriteLine(BarCode); //Print the BarCode 
            Cutting(BarCode);           //Use Cutting to split into 3 segments 
            //Print the three segments 
            Console.WriteLine(FirstSegment);   
            Console.WriteLine(SecondSegment);
            Console.WriteLine(ThirdSegment); 
            //Using Booleans function 
            Verification(); 
            //Initialize the DataBase 
            ConectionBdd();
            //Regroup the BarCode into one variable 
            if(BarCodeIsValid == true)
            {
                VerifiedBarCode = Gather(FirstSegment, SecondSegment, ThirdSegment); 
                Console.WriteLine(VerifiedBarCode);     //Print the BarCode which is now verified 
                InsertionBDD(VerifiedBarCode);
            }    
            else
                Console.WriteLine("BarCode isn't valid"); //WithDraw an Error Code if the BarCode is valid 
            DeconectionBDD();
        }
    }
}
